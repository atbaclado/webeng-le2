const express = require('express');

const router = new express.Router();

router.post('/submit', function(request, response) {
	const username = request.body.username;

	if(username !== null) {
		request.userName = username;
		response.redirect('/profile/:username');
	}else {
		response.redirect('/');
	}
});

module.exports = router;