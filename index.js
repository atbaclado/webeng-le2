var express = require('express');
var consolidate = require('consolidate');
var bodyparser = require('body-parser');

var app = express();

app.set('views', './views');
app.engine('html', consolidate.nunjucks);

app.use(bodyparser.urlencoded({ extended: false }));
app.use(express.static('./static'));

app.get('/', function(request, response) {
	var username = request.query.username;
	response.render('index.html', {
		username: username
	});
});

app.get('/profile/:username', function(request, response) {
	var username = request.params.username;
	response.render('profile.html', {
		username: username
	});
});

app.post('/submit', function(request, response) {
	const username = request.body.username;

	if(username != null) {
		request.params = { "username": username };
		response.redirect('/profile/' + request.params.username);
	}else {
		response.redirect('/');
	}
});

app.listen(3000, function() {
  console.log('Server is now listening at port 3000');
});